const fs = require('fs'); 
const parse = require('csv-parse');
const Web3 = require('web3');
const XLSX = require('xlsx');
// const Blob = require('./blob');
// const util = require('ethereumjs-util');

const web3 = new Web3(
    new Web3.providers.HttpProvider('https://mainnet.infura.io/')
);

// Read file csv
function readFile(path) {
    var data = [];
    fs.readFile(path, async (err,data)=>{
        if (!err) {
            // console.log(data.toString());
            let csvData = data.toString().replace(/^\s+|\s+$/gm,'').split(',');
            let query = [];
            for(let i=0; i < csvData.length; i++) {
                if(csvData[i] != ''){
                    query.push(
                        new Promise(async (resolve, reject) => {
                            let balance = await checkBalalanceETH(csvData[i]);
                            resolve({
                                Address: csvData[i],
                                Balance: balance,
                            })
                        })
                    );
                }
            }
            let result = await Promise.all(query);
            exportFile(result);
        }
    });
};

// Export to file csv
function exportFile(data) {
    // console.log(data);
    
    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(data);
    // console.log(ws);
    
    /* write workbook (use type 'binary') */
    var csv = XLSX.utils.sheet_to_csv(ws);

    // console.log(csv);
    fs.writeFile('output_result.csv', csv, function(err) {
        if (err) throw err;
        console.log('Done');
    });
    // FileSaver.saveAs(new Blob([s2ab(csv)],{type:"application/octet-stream"}), "output_result.csv");
};

// Check balance of ethereum address
async function checkBalalanceETH(address) {
    let balance = 0;

    let a = await new Promise((resolve, reject) => {
        web3.eth.getBalance(address, 'latest', function(err, result) {
        if (err != null) {
            // console.log("Error while retrieving the balance for address["+address+"]: "+err);
            resolve(null);
        }
        resolve(Number(web3.fromWei(result, "ether")));
        })
    });
    if(a){
        balance = a;
    }
    return balance;
};

// checkBalalanceETH('0xD551234Ae421e3BCBA99A0Da6d736074f22192FF');
readFile('20address.csv');
// console.log(csvData);